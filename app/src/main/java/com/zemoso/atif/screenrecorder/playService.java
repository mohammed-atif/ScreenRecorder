package com.zemoso.atif.screenrecorder;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class playService extends Service {
    Intent playIntent;


    public playService() {
    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Service", "In Play ");
        if(!RecorderService.playStatus) {
            Log.e("recorder status",RecorderService.playStatus+"");
            playIntent = new Intent(this, RecordControllerService.class);
            startService(playIntent);
        }else Toast.makeText(this,"Recorder already playing", Toast.LENGTH_LONG).show();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RecorderService.playStatus = false;
        stopService(playIntent);

    }
}
