package com.zemoso.atif.screenrecorder;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.projection.MediaProjectionManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //status to check whether service is running
    private boolean recorderStatus = false;
    //saved instance
    private final String RECORDER_STATUS="recorder_status";
    //permssion variables
    private final int REQUEST_CODE_CAPTURE_PERM = 12;
    private final int REQUEST_CODE_WRITE_PERM = 34;
    //checks whether record permission is given or not
    private static boolean CAPTURE_REQUEST_STATUS = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        recorderStatus = RecorderService.status;
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final FloatingActionButton play = (FloatingActionButton) findViewById(R.id.play);
        if(recorderStatus) play.setImageResource(R.drawable.ic_media_stop);
        else play.setImageResource(R.drawable.ic_media_play);

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //requesting permission to write
                if (ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_CODE_WRITE_PERM);

                }

                //requesting permission to record
                if(!CAPTURE_REQUEST_STATUS) {
                    MediaProjectionManager mMediaProjectionManager = (MediaProjectionManager) getSystemService(
                            android.content.Context.MEDIA_PROJECTION_SERVICE);
                    Intent permissionIntent = mMediaProjectionManager.createScreenCaptureIntent();
                    startActivityForResult(permissionIntent, REQUEST_CODE_CAPTURE_PERM);
                }

                if (!recorderStatus) {
                    recorderStatus = true;
                    startService(new Intent(MainActivity.this, RecorderService.class));
                    Snackbar.make(view, "Recorder Started", Snackbar.LENGTH_LONG).show();
                    play.setImageResource(R.drawable.ic_media_stop);
                } else {
                    recorderStatus = false;
                    stopService(new Intent(MainActivity.this, RecorderService.class));
                    Snackbar.make(view, "Recorder Stopped", Snackbar.LENGTH_LONG).show();
                    play.setImageResource(R.drawable.ic_media_play);
                }
            }
        });
    }



    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (REQUEST_CODE_CAPTURE_PERM == requestCode) {
            if (resultCode == RESULT_OK) {
                Log.e("capture", "capture permission given");
                CAPTURE_REQUEST_STATUS = true;
                RecordControllerService.RESULT_CODE = resultCode;
                RecordControllerService.RESULT_INTENT = intent;
            } else {
                // user did not grant permissions
                Log.e("capture", "capture permission not given");
                Toast.makeText(this, "Please accept the permission", Toast.LENGTH_LONG).show();
            }
        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_WRITE_PERM: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    Log.e("write","Write permission granted");
                    // contacts-related task you need to do.

                } else {

                    Toast.makeText(this, "Please accept the permission", Toast.LENGTH_LONG).show();

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }

    }
}
